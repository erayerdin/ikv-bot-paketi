# İstanbul Kıyamet Vakti Bot Paketi

**Sürüm:** 0.0.1

**Lisans:** WTFPL

Bu paket İKV'de bulunmayan bazı özelliklerin sağlanması ve kolaylık için geliştirilmektedir. Bu botu kullanmanız kendi sorumluluğunuz altındadır.

Bot çalışmaktadır. Sadece aktif veya iptal etmek için tuşlara birkaç kere basmanız gerekebilir, tek seferde olmayabilir.

Siz de botu geliştirip buraya commit gönderebilirsiniz ya da başka yerde paylaşabilirsiniz. Bot, WTFPL lisanslıdır. Onla ne istiyorsanız yapın.

## Şu An Bulunan Botlar

 - Savaşçı
 
## Nasıl Kullanılır?
**Temel Tuşlar**

 - **t:** Yatma.
 - **z:** Zar.
 - **F1:** Grup sohbet.
 - **F2:** Rol yapma.

### Savaşçı
 - **CTRL+e:** Otomatik Saldırı ve Sert Vuruş (3)
 - **ALT+e:** Otomatik Saldırı ve Sert Vuruş İptal
 - **CTRL+r:** Otomatik Hedef Seçme
 - **ALT+r:** Otomatik Hedef Seçme İptal