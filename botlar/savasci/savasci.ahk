/*
�stanbul - K�yamet Paketi
Bot Paketi
Ayr�nt�: readme.md

T�r: Sava���
S�r�m: 0.0.1
Son G�ncelleme: 14.08.2015

�leti�im: http://github.com/erayerdin
*/

/*
=================
Varsay�lan Tu�lar
=================
1. H�zl� Ko�ma
2. Ofansif D�v��
3. Sert Vuru�
*/

; Global De�i�ken Yok.

;;;;;;;;;;;;;;;;
;    Global    ;
; Fonksiyonlar ;
;;;;;;;;;;;;;;;;
Press(key, ms:=50) {
	/*
	Gecikmeli Tu� Basma Fonksiyonu
	key: tu�
	ms: milisaniye
	*/
	Send, {%key% down}
	Sleep, ms
	Send, {%key% up}
	return
}

;;;;;;;;;;;
; Global  ;
; Bloklar ;
;;;;;;;;;;;
Autoattack:
	Press("space")
	return

Autohardhit:
	Press("3")
	return

Autopick:
	Send, {" down}
	Sleep, 50
	Send, {" up}
	return

;;;;;;;;;;
; Temel  ;
; Tu�lar ;
;;;;;;;;;;

; Yatma ;
t::
	Press("enter")
	Send, /yat
	Press("enter")
	return

; Zar ;
z::
	Press("enter")
	Send, /zar
	Press("enter")
	return

; Grup Sohbet
F1::
	Press("enter")
	Send, /g{space}
	return
	
; Rol Yapma
F2::
	Press("enter")
	Send, /rol{space}
	return

;;;;;;;;;;
;  �zel  ;
; Tu�lar ;
;;;;;;;;;;

; Otomatik Sald�rma ve Sert Vuru� ;
^e::
	SetTimer, Autoattack, 1000
	SetTimer, Autohardhit, 8000
	return

!e::
	SetTimer, Autoattack, OFF
	SetTimer, Autohardhit, OFF
	return

; Otomatik Hedef Se�me ;
^r::
	SetTimer, Autopick, 5000
	return

!r::
	SetTimer, Autopick, OFF
	return